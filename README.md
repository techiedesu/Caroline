# Caroline

Telegram bot small home project. 

## Getting Started

You can run Caroline as .Net Core 2.0 app or build docker image. Otherwise, you should set TELEGRAM_API_KEY
and optionally OPENWEATHERMAP_API_KEY in your enviroment varible.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.
