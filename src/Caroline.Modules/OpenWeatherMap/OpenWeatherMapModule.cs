﻿using System;
using Caroline.Interfaces;
using Caroline.Interfaces.Modules.OpenWeatherMap;
using Caroline.Modules.Core.Interfaces;

namespace Caroline.Modules.OpenWeatherMap
{
    public class OpenWeatherMapModule : IModule, IOpenWeatherMapModule
    {
        protected Api Api;

        public string Name => throw new NotImplementedException();
        public string Author => throw new NotImplementedException();
        public string ConfigKey => "OPENWEATHERMAP_API_KEY";

        public OpenWeatherMapModule(IConfigModule configModule)
        {
            if(!configModule.IsExists(ConfigKey))
                throw new Exception($"Can't find {ConfigKey} key in configuration!");

            Api = new Api(configModule.Get(ConfigKey));
        }

        public IAnswer GetCityByName(string name, string language)
        {
            return Api.GetCityByName(name, language);
        }
    }
}
