﻿using Newtonsoft.Json;

namespace Caroline.Modules.OpenWeatherMap.Models
{
    /// <summary>
    /// (more info WeatherApi condition codes)
    /// </summary>
    public class Weather
    {
        /// <summary>
        /// WeatherApi condition id
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Group of weather parameters (Rain, Snow, Extreme etc.)
        /// </summary>
        [JsonProperty("main")]
        public string Main { get; set; }

        /// <summary>
        /// WeatherApi condition within the group
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// WeatherApi icon id
        /// </summary>
        [JsonProperty("icon")]
        public string Icon { get; set; }
    }
}
