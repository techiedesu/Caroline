﻿using Newtonsoft.Json;

namespace Caroline.Modules.OpenWeatherMap.Models
{
    public class Coord
    {
        /// <summary>
        ///  City Geo location, longitude
        /// </summary>
        [JsonProperty("lon")]
        public double Lon { get; set; }

        /// <summary>
        /// City Geo location, latitude
        /// </summary>
        [JsonProperty("lat")]
        public double Lat { get; set; }
    }
}
