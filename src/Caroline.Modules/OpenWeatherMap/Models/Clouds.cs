﻿using Newtonsoft.Json;

namespace Caroline.Modules.OpenWeatherMap.Models
{
    public class Clouds
    {
        /// <summary>
        /// Cloudiness, %
        /// </summary>
        [JsonProperty("all")]
        public int All { get; set; }
    }
}
