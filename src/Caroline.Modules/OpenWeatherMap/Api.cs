﻿using System;
using System.Linq;
using System.Net.Http;
using Caroline.Modules.OpenWeatherMap.Models;
using Newtonsoft.Json;

namespace Caroline.Modules.OpenWeatherMap
{
    public class Api
    {
        public string DefaultCity { get; set; }
        public string DefaultLanguage { get; set; }

        readonly string _apiKey;

        /// <summary>
        /// OpenWeatherMap constructor.
        /// </summary>
        /// <param name="apiKey">You can get it here: https://home.openweathermap.org/api_keys </param>
        /// <param name="defaultCity">Default city</param>
        /// <param name="defaultLanguage">Default language</param>
        public Api(string apiKey, string defaultCity = "", string defaultLanguage = "")
        {
            _apiKey = apiKey;
            DefaultCity = defaultCity.Length != 0 ? defaultCity : "Moscow";
            DefaultLanguage = defaultLanguage.Length != 0 ? defaultLanguage : "ru";
        }

        public bool IsAvailable()
        {
            return !(string.IsNullOrEmpty(_apiKey) || GetCityByName(DefaultCity, DefaultLanguage) == null);
        }

        public Answer GetCityByName(string name, string lang = "")
        {
            if (lang == "") lang = DefaultLanguage;
            var data = MakeRequest(
                $"http://api.openweathermap.org/data/2.5/weather?q={name}&APPID={_apiKey}&lang={lang}");
            try
            {
                return JsonConvert.DeserializeObject<Answer>(data);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex + "\n" + data);
                return null;
            }
        }

        protected string MakeRequest(string addr)
        {
            try
            {
                var hc = new HttpClient();
                var x = hc.GetStringAsync(addr).Result;

                return x;
            }
            catch
            {
                return null;
            }
        }
    }
}
