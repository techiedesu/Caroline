﻿using System.Linq;
using Caroline.Modules.OpenWeatherMap.Models;

namespace Caroline.Modules.OpenWeatherMap
{
    public class Pretty
    {
        private readonly Answer _answer;

        public Pretty(Answer answer)
        {
            _answer = answer;
        }

        public string Icon()
        {
            switch (_answer.Weather.FirstOrDefault()?.Icon)
            {
                // clear sky
                case "01d":
                case "01n":
                    return "☀️";
                // few clouds
                case "02d":
                case "02n":
                    return "🌤";
                // scattered clouds
                case "03d":
                case "03n":
                    return "⛅️";
                // broken clouds
                case "04d":
                case "04n":
                    return "🌥";
                // shower rain
                case "09d":
                case "09n":
                    return "🌧";
                // rain
                case "10d":
                case "10n":
                    return "🌦";
                // thunderstorm
                case "11d":
                case "11n":
                    return "⛈";
                // snow
                case "13d":
                case "13n":
                    return "🌨";
                // mist
                case "50d":
                case "50n":
                    return "🌫";
            }

            return $"({_answer.Weather.FirstOrDefault()?.Description})";
        }

        public string Temp(bool isKelvin)
        {
            return $"{(int)(_answer.Main.Temp - (isKelvin ? 273.15 : 0))}";
        }

        public override string ToString()
        {
            try
            {
                var desc = _answer.Weather.FirstOrDefault()?.Description;

                if (desc != null && desc.Length > 1)
                {
                    desc = desc[0].ToString().ToUpper() + desc.Substring(1);
                }

                // TODO: add kelvin/celsius selector
                // TODO: add update time
                
                return $"{_answer.Name} {Icon()}\n" +
                       $"🌡: {Temp(isKelvin: false)} °C\\{Temp(isKelvin: true)} °K\n" +
                       $"💨: {_answer.Wind.Speed} m/s";
            }
            catch
            {
                return "I've got wrong answer. Try later again.";
            }
        }
    }
}
