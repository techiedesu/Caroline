﻿using System;
using System.Linq;
using System.Text;
using Caroline.Interfaces;

namespace Caroline.Modules.Daily
{
    public class DailyTelegram
    {
        protected IDataRepository<Models.Daily> DailyDataRepository;

        public DailyTelegram(IDataRepository<Models.Daily> dailyDataRepository)
        {
            DailyDataRepository = dailyDataRepository;
        }

        /// <summary>
        /// Add money to user
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="count"></param>
        /// <param name="updateGiveTime"></param>
        /// <returns>Success</returns>
        public void AddMoney(long userId, uint count, bool updateGiveTime)
        {
            var user = DailyDataRepository.GetBy(x => x.UserId == userId).First();

            user.Cash += count;
            if (updateGiveTime) user.UpdateTime = DateTime.Now;
        }

        public (bool, string) GiveUserDailyMoney(long userId)
        {
            var user = DailyDataRepository.GetBy(x => x.UserId == userId).FirstOrDefault();

            if (user == null || user.UpdateTime <= DateTime.Now.AddDays(-1))
            {
                AddMoney(userId, 200, true);
                return (true, "You received your 💴 200 daily credits!");
            }

            var time = FormatDateTime(user.UpdateTime.AddDays(1) - DateTime.Now);

            return (false, $"You should wait around {time}");
        }

        public ulong GetBalance(long userId)
        {
            var user = DailyDataRepository.GetBy(x => x.UserId == userId).FirstOrDefault();
            return user?.Cash ?? 0;
        }

        protected string FormatDateTime(TimeSpan datetime)
        {
            var ret = new StringBuilder();
            if (datetime.Days > 0)
            {
                ret.Append(datetime.Days);
                ret.Append(" day");
                if (datetime.Days > 1)
                {
                    ret.Append("s");
                }
                ret.Append(" ");
            }

            if (datetime.Hours > 0)
            {
                ret.Append(datetime.Hours);
                ret.Append(" hour");
                if (datetime.Hours > 1)
                {
                    ret.Append("s");
                }
                ret.Append(" ");
            }

            if (datetime.Minutes > 0)
            {
                ret.Append(datetime.Minutes);
                ret.Append(" minute");
                if (datetime.Minutes > 1)
                {
                    ret.Append("s");
                }
                ret.Append(" ");
            }

            return ret.ToString();
        }
    }
}
