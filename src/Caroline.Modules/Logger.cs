﻿using System;
using Microsoft.Extensions.Logging;

namespace Caroline.Modules
{
    public class Logger<T> : Interfaces.Modules.ILogger<T>
    {
        protected Microsoft.Extensions.Logging.Logger<T> MicrosoftLogger;

        public Logger(Interfaces.Modules.ILoggerFactory factory)
        {
            MicrosoftLogger = new Microsoft.Extensions.Logging.Logger<T>(factory);
        }

        public IDisposable BeginScope(string state, params object[] args)
        {
            return MicrosoftLogger.BeginScope(state, args);
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            throw new NotImplementedException();
        }

        public void LogCritical(string message, params object[] args)
        {
            MicrosoftLogger.LogCritical(message, args);
        }

        public void LogError(string message, params object[] args)
        {
            MicrosoftLogger.LogError(message, args);
        }

        public void LogInformation(string message, params object[] args)
        {
            MicrosoftLogger.LogInformation(message, args);
        }

        public void LogWarning(string message, params object[] args)
        {
            MicrosoftLogger.LogWarning(message, args);
        }
    }
}
