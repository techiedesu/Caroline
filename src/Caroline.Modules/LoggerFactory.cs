﻿using Microsoft.Extensions.Logging;
using ILoggerFactory = Microsoft.Extensions.Logging.ILoggerFactory;

namespace Caroline.Modules
{
    public class LoggerFactory : Interfaces.Modules.ILoggerFactory
    {
        protected ILoggerFactory MicrosoftLoggerFactory;

        public LoggerFactory()
        {
            MicrosoftLoggerFactory = new Microsoft.Extensions.Logging.LoggerFactory();
            MicrosoftLoggerFactory.AddConsole();
        }

        public void AddProvider(ILoggerProvider provider)
        {
            MicrosoftLoggerFactory.AddProvider(provider);
        }

        public ILogger CreateLogger(string categoryName)
        {
            return MicrosoftLoggerFactory.CreateLogger(categoryName);
        }

        public void Dispose()
        {
            MicrosoftLoggerFactory.Dispose();
        }
    }
}
