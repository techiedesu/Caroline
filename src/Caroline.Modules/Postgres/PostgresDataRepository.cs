﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Caroline.Database;
using Caroline.Interfaces;

namespace Caroline.Modules.Postgres
{
    public class PostgresDataRepository<T> : IDataRepository<T> where T : class
    {
        private readonly BotContext _ctx;

        public PostgresDataRepository()
        {
            _ctx = new BotContext();
        }

        public void Create(T entity)
        {
            var set = _ctx.Set<T>();
            set.Add(entity);
        }

        public void Update(T entity)
        {
            var set = _ctx.Set<T>();
            set.Update(entity);
        }

        public void Delete(T entity)
        {
            var set = _ctx.Set<T>();
            set.Remove(entity);
        }

        public void Dispose()
        {
            _ctx.SaveChanges();
            _ctx?.Dispose();
        }

        public IQueryable<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            var set = _ctx.Set<T>();
            return set.Where(predicate);
        }
    }
}
