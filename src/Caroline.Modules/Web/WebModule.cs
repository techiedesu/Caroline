﻿using Caroline.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;

namespace Caroline.Modules.Web
{
    public class WebModule : IModule
    {
        public string Name => "Web Module";
        public string Author => "Me";

        public string ConfigKey => throw new System.NotImplementedException();

        public WebModule(string[] args = null)
        {
            var x = BuildWebHost(args).RunAsync();
        }

        public static IWebHost BuildWebHost(string[] args = null) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
