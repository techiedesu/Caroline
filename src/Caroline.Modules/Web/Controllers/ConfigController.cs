﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Caroline.Modules.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ConfigController : Controller
    {
        public ConfigController()
        {

        }

        [AllowAnonymous]
        [Route("1")]
        public bool GetIndex()
        {
            return true;
        }
    }
}
