﻿using Caroline.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Caroline.Database
{
    public class BotContext : DbContext
    {
        public string ConnectionString;

        public BotContext()
        {
            ConnectionString = Environment.GetEnvironmentVariable("CAROLINE_DB_CS");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(ConnectionString);
            }
        }

        public DbSet<Daily> Daily { get; set; }
    }
}
