﻿using Microsoft.EntityFrameworkCore.Design;

namespace Caroline.Database
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<BotContext>
    {
        BotContext IDesignTimeDbContextFactory<BotContext>.CreateDbContext(string[] args)
        {
            return new BotContext();
        }
    }
}
