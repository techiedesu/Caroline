﻿using System;
using System.Collections.Generic;
using Caroline.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Caroline.Helpers
{
    public class HCommand
    {
        protected static Dictionary<string, Type> CommandsCollection;

        public static List<ICommand> SortByBootOrder(List<ICommand> commandsList)
        {
            for (var i = 0; i < commandsList.Count; ++i)
            {
                commandsList.Sort((x, y) => x.BootOrder().CompareTo(y.BootOrder()));
            }
            return commandsList;
        }

        public static bool InitCollection(List<ICommand> commandsList, 
            Action<ICommand, string> onSuccess, Action<ICommand, string> onError)
        {
            CommandsCollection = new Dictionary<string, Type>();

            foreach(var command in commandsList)
            {
                foreach(var triggerCommand in command.EventCommands())
                {
                    if(CommandsCollection.ContainsKey(triggerCommand))
                    {
                        onError(command, triggerCommand);
                        return false;
                    }

                    CommandsCollection.Add(triggerCommand, command.GetType());
                    onSuccess(command, triggerCommand);
                }
            }

            return true;
        }

        public static ICommand QueryObjectCommand(string triggerCommand, IServiceProvider di)
        {
            if (CommandsCollection.ContainsKey(triggerCommand))
            {
                return di.GetRequiredService(CommandsCollection[triggerCommand]) as ICommand;
            }

            return null;
        }
    }
}
