﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Caroline.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Caroline.Helpers
{
    public static class AssemblyHelper
    {
        public static List<Type> GetBasedTypes<T>(Assembly assembly)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(t => t.GetTypes())
                .Where(
                    t => t.IsClass
                         && (t.Namespace ?? "").StartsWith("Caroline.Commands")
                         && typeof(T).IsAssignableFrom(t)
                         && !t.IsAbstract
                         && !t.IsInterface
                         );

            return assemblies.ToList();
        }

        public static List<TBase> GetInstances<TBase>(List<Type> typesList, IServiceProvider di) where TBase : class, ICommand
        {
            var instancesList = new List<TBase>();

            foreach (var currentType in typesList)
            {
                var newInstance = di.GetRequiredService(currentType) as TBase;
                instancesList.Add(newInstance);
            }

            return instancesList;
        }
    }
}
