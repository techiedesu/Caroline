﻿using System;

namespace Caroline.Helpers
{
    public class ConfigurationHelper
    {

        public string this[string index, string defaultValue = ""]
        {
            get => Environment.GetEnvironmentVariable(index) ?? defaultValue;
            set => Environment.SetEnvironmentVariable(index, value);
        }

        public bool IsExists(string index) => Environment.GetEnvironmentVariable(index) != null;
    }
}
