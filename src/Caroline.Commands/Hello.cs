﻿using Caroline.Commands.Abstracts;
using Caroline.Helpers;
using Caroline.Interfaces;

namespace Caroline.Commands
{
    public class Hello : Command<Hello>
    {
        public Hello()
        {

        }

        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "hi", "sup", "hai", "hello" };
        }

        public override string Description => "Just hello command.";

        public override string Help => "enter `/<command>` to get beautiful answer!";

        public override string Exec(IMessage message)
        {
            var answerTo = "member";
            if (!string.IsNullOrEmpty(message.Sender.Username))
            {
                answerTo = $"@{message.Sender.Username}";
            }
            else if (!Common.IsNullOrEmpty(LogicHelper.Or, message.Sender.FirstName, message.Sender.LastName))
            {
                answerTo = $"{message.Sender.FirstName} {message.Sender.LastName}";
            }
            else if (!string.IsNullOrEmpty(message.Sender.FirstName))
            {
                answerTo = message.Sender.FirstName;
            }

            var result = Common.WriteLine($"Hello, {answerTo}!");

            return result;
        }

    }
}
