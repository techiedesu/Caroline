﻿using System.Threading.Tasks;
using Caroline.Interfaces;
using Caroline.Modules.Daily;

namespace Caroline.Commands
{
    public class Shop : ICommand<Shop>
    {
        protected DailyTelegram DailyModule;

        public Shop(DailyTelegram dailyTelegram)
        {
            DailyModule = dailyTelegram;
        }

        public bool Init(object data)
        {
            return true;
        }

        public string Description => "Shop provider.";

        public string Help => "enter `/<command>` to get daily cash";

        public string Author => "V.A. (@techiedesu)";

        public bool IsHidden => false;

        public string[] EventCommands()
        {
            return new[] { "shop" };
        }

        public string Exec(IMessage message)
        {
            var result = "Shop\r\n\r\n\r\n💴: ";
            result += DailyModule.GetBalance(message.Sender.Id);

            return result;
        }

        public string ExecMarkdown(IMessage message)
        {
            return "Dont";
        }

        public int BootOrder()
        {
            return 2;
        }

        public Task<string> ExecAsync(IMessage message)
        {
            return Task.Run(() => Exec(message));
        }
    }
}
