﻿using Caroline.Commands.Abstracts;
using Caroline.Interfaces;

using System.Globalization;
using System.Net.NetworkInformation;

namespace Caroline.Commands
{
    public class Ping : Command<Ping>
    {
        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "ping" };
        }

        public override string Description => "Ping reply checker.";

        public override string Help => "enter `/<command> <host>` to check is alive host or not";

        public override string Author => "V.A. (@techiedesu)";

        public override string Exec(IMessage message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);
            var host = "mail.ru";

            if (splitted.Length > 1)
            {
                host = splitted[1];
            }

            return PingHost(host);
        }

        private static string PingHost(string nameOrAddress)
        {
            System.Net.NetworkInformation.Ping pinger = new System.Net.NetworkInformation.Ping();
            var idn = new IdnMapping();
            nameOrAddress = idn.GetAscii(nameOrAddress);
            try
            {
                PingReply reply = pinger.SendPingAsync(nameOrAddress).Result;

                return  $"{reply.Address} is alive! Time: {reply.RoundtripTime} ms";
            }
            catch
            {
                return "Host is dead";
            }
        }
    }
}
