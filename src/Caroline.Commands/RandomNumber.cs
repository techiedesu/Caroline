﻿using System;
using Caroline.Commands.Abstracts;
using Caroline.Interfaces;

namespace Caroline.Commands
{
    public class RandomNumber : Command<RandomNumber>
    {
        readonly Random _random = new Random();
        public override bool Init(object data) => true;

        public override string[] EventCommands() => new[] { "rand", "random" };

        public override string Exec(IMessage message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);

            switch (splitted.Length)
            {
                case 2:
                {
                    int.TryParse(splitted[1], out var value1);
                    return _random.Next(value1).ToString();
                }
                case 3:
                {
                    int.TryParse(splitted[1], out var value1);
                    int.TryParse(splitted[2], out var value2);
                    
                    if(value1 > value2)
                    {
                        return _random.Next(value2, value1 + 1).ToString();
                    }

                    return _random.Next(value1, value2 + 1).ToString();
                }
            }
            return _random.Next(int.MaxValue).ToString();
        }

        public override string Description => "Just simple random number generator.";

        public override string Author => "V.A. (@techiedesu)";

        public override string Help => "enter `/random <value_from> <value_to>` to get randomization result!";
    }
}
