﻿using System;
using System.Collections.Generic;
using System.Linq;
using Caroline.Commands.Abstracts;
using Caroline.Interfaces;

namespace Caroline.Commands
{
    public class SystemHelp : Command<SystemHelp>
    {
        private List<ICommand> _commands = new List<ICommand>();

        public override bool Init(object data)
        {
            try
            {
                if (data != null)
                {
                    var inputCommands = (List<ICommand>) data;
                    Console.WriteLine("Registered command modules count: {0}", inputCommands.Count);

                    _commands = inputCommands.Where(x =>
                    {
                        var status = !x.IsHidden ? "in public list" : "hidden from public list";
                        Console.WriteLine("HCommand {1} - {0}", x.GetType(), status);
                        return !x.IsHidden;
                    }).ToList();

                    return true;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return false;
        }

        public override string[] EventCommands()
        {
            return new[] { "help", "start" };
        }

        public override string Description => "Like `man` just help";

        public override string Help => "enter `/<command>` to get commands list";

        public override string Author => "V.A. (@techiedesu)";

        public override string Exec(IMessage message)
        {
            throw new NotImplementedException("Not today! Use markdown.");
        }

        public override string ExecMarkdown(IMessage message)
        {
            var result = "Commands list: " + Environment.NewLine;

            for (var i = 0; i < _commands.Count; i++)
            {
                var c = _commands[i];
                result += $"{i + 1}. {string.Join(", ", c.EventCommands())} - {c.Help}{Environment.NewLine}";
            }

            return result;
        }

        public override int BootOrder() => 5;
    }
}
