﻿using CodeHollow.FeedReader;
using System;
using System.Globalization;
using System.Text;
using Caroline.Interfaces;
using Caroline.Commands.Abstracts;

namespace Caroline.Commands.News
{
    public class NewsCommand : Command<NewsCommand>
    {
        public override bool Init(object data) => true;

        public override string[] EventCommands() => new[] { "news" };

        public override string Exec(IMessage message)
        {
            var sb = new StringBuilder();
            var feed = FeedReader.ReadAsync("https://news.yandex.ru/computers.rss").Result;
            sb.Append("Яндекс.Новости\r\n");
            if (feed.LastUpdatedDate != null)
            {
                var time = ((DateTime) feed.LastUpdatedDate).ToString("g", CultureInfo.CreateSpecificCulture("ru-RU"));
                sb.Append($"Последнее обновление: {time} GTM\r\n\r\n");
            }

            using (var items = feed.Items.GetEnumerator())
            {
                var i = 0;
                
                while (items.MoveNext() && i++ < 10)
                {
                    sb.Append($"{i}. <a href=\"{items.Current.Link}\">{items.Current.Title}</a>\r\n");
                }
            }
            return sb.ToString();
        }

        public override bool IsHidden => true;
    }
}
