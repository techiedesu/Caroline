﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Caroline.Commands.News.Models
{
    public class Item
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("link")]
        public string Link { get; set; }
        [JsonProperty("guid")]
        public string Guid { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("pubDate")]
        public string PubDate { get; set; }
    }
}
