﻿using Newtonsoft.Json;

namespace Caroline.Commands.News.Models
{
    public class Image
    {
        [JsonProperty("url")]
        public string Url { get; set; }
        [JsonProperty("link")]
        public string Link { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
    }
}
