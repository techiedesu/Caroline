﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Caroline.Commands.News.Models
{
    public class Channel
    {
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("link")]
        public string Link { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("image")]
        public Image Image { get; set; }
        [JsonProperty("item")]
        public List<Item> Items { get; set; }
        [JsonProperty("lastBuildDate")]
        public string LastBuildDate { get; set; }
    }
}