﻿using Caroline.Commands.Abstracts;
using Caroline.Interfaces;

using System.Globalization;

namespace Caroline.Commands
{
    public class Punycode : Command<Punycode>
    {
        public override bool Init(object data)
        {
            return true;
        }

        public override string[] EventCommands()
        {
            return new[] { "puny", "punycode" };
        }

        public override string Exec(IMessage message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);

            if (splitted.Length < 2) return "Wrong arguments.";

            var idn = new IdnMapping();

            return idn.GetAscii(splitted[1]) == splitted[1] 
                ? idn.GetUnicode(splitted[1]) : idn.GetAscii(splitted[1]);
        }

        public override string Description => "";

        public override string Author => "V.A. (@techiedesu)";

        public override string Help => "enter `/<command> <domain>` to get readable domain name or not";
    }
}
