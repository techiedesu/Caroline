﻿using System;
using System.Text;
using System.Threading.Tasks;
using Caroline.Interfaces;
using Caroline.Modules.Daily;

namespace Caroline.Commands
{
    // NekoTech, you received your :yen: 200 daily credits!
    public class Daily : ICommand<Daily>
    {
        protected DailyTelegram DailyTelegram;

        public Daily(DailyTelegram dailyTelegram)
        {
            DailyTelegram = dailyTelegram;
        }

        public string Description => "Daily cash provider.";

        public string Help => "enter `/<command>` to get daily cash";

        public bool IsHidden => false;

        public int BootOrder()
        {
            return 2;
        }

        public string[] EventCommands()
        {
            return new[] { "daily" };
        }


        public string Exec(IMessage message)
        {
            var userId = message.Sender.Id;
            var result = DailyTelegram.GiveUserDailyMoney(userId);

            return result.Item2;
        }

        public Task<string> ExecAsync(IMessage message)
        {
            return Task.Run(() => Exec(message));
        }

        public string ExecMarkdown(IMessage message)
        {
            return "Dont";
        }

        public bool Init(object data)
        {
            return true;
        }
    }
}
