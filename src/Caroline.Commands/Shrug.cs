﻿using Caroline.Commands.Abstracts;
using Caroline.Interfaces;

namespace Caroline.Commands
{
    public class Shrug : Command<Shrug>
    {
        public override bool Init(object data) => true;

        public override string[] EventCommands() => new[] { "shrug" };

        public override string Description => "Just shrug command.";

        public override string Help => "enter `/<command>` to get beautiful answer!";

        public override string Author => "Vladislav Dyshakov (@techiedesu)";

        public override string Exec(IMessage message) => @"¯\_(ツ)_/¯";

    }
}
