﻿using System.Threading.Tasks;
using Caroline.Interfaces;
using Caroline.Interfaces.Modules;

namespace Caroline.Commands.Abstracts
{
    public abstract class Command<T> : ICommand where T : Command<T>, new()
    {
        protected static T Instance;
        protected ILogger<T> Logger;

        public static T GetInstance => Instance ?? (Instance = new T());

        public void SetLogger(ILogger<T> logger)
        {
            Logger = logger;
        }

        public abstract bool Init(object data);

        public abstract string[] EventCommands();

        public abstract string Exec(IMessage message);

        public virtual string ExecMarkdown(IMessage message) => "Dont";

        public virtual string Description => string.Empty;

        public virtual string Author => string.Empty;

        public virtual bool IsHidden => false;

        public virtual string Help => string.Empty;

        public virtual Task<string> ExecAsync(IMessage message)
        {
            return Task.Run(() => Exec(message));
        }

        public virtual int BootOrder() => 2;
    }
}
