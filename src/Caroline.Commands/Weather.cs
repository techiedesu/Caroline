﻿using System;
using Caroline.Commands.Abstracts;
using Caroline.Interfaces;
using Caroline.Modules.OpenWeatherMap;

namespace Caroline.Commands
{
    public class Weather : Command<Weather>
    {
        protected Api WeatherApi;

        public override bool Init(object data)
        {
            var apiKey = Environment.GetEnvironmentVariable("OPENWEATHERMAP_API_KEY");


            if (string.IsNullOrEmpty(apiKey))
            {
                return false;
            }

            WeatherApi = new Api(apiKey);

            return WeatherApi.IsAvailable();
        }

        public override string Description => "OpenWeatherMap provider.";

        public override string Help => "enter `/<command> <city>` to get weather";

        public override string Author => "V.A. (@techiedesu)";

        public override string[] EventCommands()
        {
            return new[] { "weather" };
        }

        public override string Exec(IMessage message)
        {
            var splitted = Helpers.Common.ParseCommandArgs(message.Body);
            var city = "Novosibirsk";

            if (splitted.Length > 1)
            {
                city = splitted[1];
            }

            var curWeather = WeatherApi.GetCityByName(city, "en");
            return new Pretty(curWeather).ToString();
        }
    }
}
