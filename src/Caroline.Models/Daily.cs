﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Caroline.Models
{
    public class Daily
    {
        [Key]
        public long UserId { get; set; }
        public ulong Cash { get; set; }
        public DateTime UpdateTime { get; set; }

    }
}
