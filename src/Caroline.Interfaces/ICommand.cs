﻿namespace Caroline.Interfaces
{
    public interface ICommand<out T> : ICommand
    {
    }
}
