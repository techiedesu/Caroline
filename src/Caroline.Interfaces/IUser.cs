using System;

namespace Caroline.Interfaces
{
    public interface IUser
    {
        Int64 Id { get;set; }
        string Username { get;set; }
        string FirstName { get;set; }
        string LastName { get;set; }

    }
}
