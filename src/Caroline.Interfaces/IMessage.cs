using System;

namespace Caroline.Interfaces
{
    public interface IMessage
    {
        int Id { get; set; }
        string Body { get; set; }
        IUser Sender { get; set; }
        bool IsChat { get; set; }
        DateTime PostTime { get; set; }
        Int64 ChatId { get; set; }
        bool IsAnswer { get; }
        IMessage AnswerTo { get; set; }
    }
}
