﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Caroline.Interfaces
{
    public interface IDataRepository<T> : IDataRepository, IDisposable
        where T : class
    {
        void Create(T element);
        void Update(T element);
        void Delete(T element);

        // , params Expression<Func<T, object>>[] includes
        IQueryable<T> GetBy(Expression<Func<T, bool>> predicate);
    }
}
