﻿using System.Threading.Tasks;
using Caroline.Interfaces;

namespace Caroline.Interfaces
{
    public interface ICommand
    {
        bool Init(object data);
        string[] EventCommands();
        string Exec(IMessage message);
        string ExecMarkdown(IMessage message);
        string Description { get; }

        int BootOrder();
        
        bool IsHidden { get; }
        string Help { get; }

        Task<string> ExecAsync(IMessage message);
    }
}
