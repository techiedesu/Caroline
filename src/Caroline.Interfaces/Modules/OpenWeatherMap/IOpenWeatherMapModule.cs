﻿using Caroline.Interfaces.Modules.OpenWeatherMap;

namespace Caroline.Interfaces
{
    public interface IOpenWeatherMapModule
    {
        IAnswer GetCityByName(string name, string language);
    }
}
