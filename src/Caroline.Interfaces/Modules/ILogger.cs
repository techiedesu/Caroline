﻿using System;

namespace Caroline.Interfaces.Modules
{
    public interface ILogger : Microsoft.Extensions.Logging.ILogger
    {
        IDisposable BeginScope(string state, params object[] args);
        void LogCritical(string message, params object[] args);
        void LogError(string message, params object[] args);
        void LogWarning(string message, params object[] args);
        void LogInformation(string message, params object[] args);
    }
}
