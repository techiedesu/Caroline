﻿namespace Caroline.Interfaces.Modules
{
    public interface IConfigModule
    {
        string Get(string key);
        bool IsExists(string key);
        bool Set(string key, string value);
    }
}
