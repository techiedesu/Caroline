﻿namespace Caroline.Interfaces.Modules
{
    public interface ILogger<out TCategoryName> : Microsoft.Extensions.Logging.ILogger<TCategoryName>, ILogger
    {

    }
}
