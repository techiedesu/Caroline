﻿namespace Caroline.Interfaces
{
    public interface IModule
    {
        string Name { get; }
        string Author { get; }
        string ConfigKey { get; }
    }
}
