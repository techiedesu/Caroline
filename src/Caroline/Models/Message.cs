﻿using System;

namespace Caroline.Models
{
    public class Message : Interfaces.IMessage
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public Interfaces.IUser Sender { get; set; }
        public bool IsChat { get; set; }
        public DateTime PostTime { get; set; }
        public long ChatId { get; set; }
        public bool IsAnswer => AnswerTo != null;
        public Interfaces.IMessage AnswerTo { get; set; }
    }
}
