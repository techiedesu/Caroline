﻿namespace Caroline.Models
{
    public class User : Interfaces.IUser
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
