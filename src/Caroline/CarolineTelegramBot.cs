﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Caroline.Commands;
using Caroline.Converters;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Caroline.Helpers;
using Caroline.Interfaces;
using Caroline.Interfaces.Modules;

namespace Caroline
{
    public class CarolineTelegramBot
    {
        protected TelegramBotClient Bot;
        protected int MyId;
        protected readonly ConfigurationHelper Config;
        protected readonly ILogger<CarolineTelegramBot> Logger;

        public List<ICommand> InterfaceBasedCommands { get; }

        public CarolineTelegramBot(ConfigurationHelper config, ILogger<CarolineTelegramBot> logger)
        {
            Config = config;
            Logger = logger;
            InterfaceBasedCommands = new List<ICommand>();
        }

        protected void Init()
        {
            MyId = Bot.GetMeAsync().Result.Id;
            RegisterCommands();
            RegisterEvents();
        }

        protected bool CheckKeys()
        {
            if (!Config.IsExists("TELEGRAM_API_KEY"))
            {
                Logger.LogCritical("Telegram key is empty");
                return false;
            }

            return true;
        }

        public void Start()
        {
            if (!CheckKeys()) return;
            
            Bot = new TelegramBotClient(Config["TELEGRAM_API_KEY"]);
            Init();
            Logger.LogInformation("Bot name is: @{0}", Bot.GetMeAsync().Result.Username);

            Bot.StartReceiving();
        }

        protected void RegisterEvents()
        {
            Bot.OnMessage += async (o, s) => await Bot_OnMessageAsync(s);
        }

        protected async Task Bot_OnMessageAsync(MessageEventArgs e)
        {
            var msg = e.Message;
            Logger.LogInformation($"({msg.Chat.Id}) ({msg.From.Id}) {msg.Date} {msg.From.FirstName}: {msg.Text}");

            var isItMe = MyId == msg.From.Id;

            if (string.IsNullOrWhiteSpace(msg?.Text) || isItMe)
            {
                return;
            }

            await AnswerToMessageAsync(e.Message).ConfigureAwait(false);
        }

        private Dictionary<string, ICommand> _handlers;
 
        private void RegisterCommands()
        {
            _handlers = new Dictionary<string, ICommand>();

            var commandTypes = AssemblyHelper.GetBasedTypes<ICommand>(System.Reflection.Assembly.GetExecutingAssembly());
            var commandInstances = AssemblyHelper.GetInstances<ICommand>(commandTypes, Program.di);
            commandInstances = HCommand.SortByBootOrder(commandInstances);

            var commandInstancesActive = new List<ICommand>(commandInstances);

            foreach (var instance in commandInstances)
            {
                var instanceType = instance.GetType();
                var data = instanceType == typeof(SystemHelp) ? commandInstancesActive : null;

                var initResult = instance.Init(data);


                if (!initResult)
                {
                    Logger.LogError("{0} failed to load. Skipping...", instanceType);
                    commandInstancesActive.Remove(instance);
                    continue;
                }
                Logger.LogInformation("{0} has been loaded.", instanceType);
            }


            void OnSuccess(ICommand command, string trigger)
            {
                Logger.LogInformation("Command `{0}` from `{1}` has been registered.", trigger, command.GetType());
            }

            void OnError(ICommand command, string trigger)
            {
                Logger.LogInformation("Fatal error. Can't add command {0} from {1}", trigger, command.GetType());
            }

            if (HCommand.InitCollection(commandInstancesActive, OnSuccess, OnError))
            {
                Console.WriteLine("Commands have been registered");
            }
        }

        private async Task AnswerToMessageAsync(Message tgMessage)
        {
            var message = TelegramConverter.Message(tgMessage);

            var spilledStrMessage = message.Body.ToLower().Split(' ');
            var command = spilledStrMessage[0].Remove(0, 1);

            var username = "@" + (await Bot.GetMeAsync().ConfigureAwait(true)).Username.ToLower();
            if (command.Contains(username))
            {
                command = command.Split('@')[0];
            }

            var triggerCommand = HCommand.QueryObjectCommand(command, Program.di);

            if (triggerCommand != null)
            {
                var answer = triggerCommand.ExecMarkdown(message);
                var parseMode = ParseMode.Markdown;
                
                if(answer == @"Dont")
                {
                    answer = await triggerCommand.ExecAsync(message).ConfigureAwait(false);
                    parseMode = ParseMode.Html;
                }

                if (string.IsNullOrWhiteSpace(answer)) return;
                try
                {

                    await Bot.SendTextMessageAsync(message.ChatId, answer, replyToMessageId: message.Id,
                        parseMode: parseMode).ConfigureAwait(false);
                }
                catch(Exception ex)
                {
                    Logger.LogWarning("Message {0} doesn't answered. Maybe deleted?{1}{2}", message.Id, Environment.NewLine, ex);
                }
            }
        }

        public void Stop()
        {
            Bot.StopReceiving();
        }
    }
}
