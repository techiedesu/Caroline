﻿using System;
using System.Threading;
using Caroline.Commands;
using Caroline.Commands.News;
using Caroline.Helpers;
using Caroline.Interfaces;
using Caroline.Models;
using Caroline.Modules.Daily;
using Caroline.Modules.Postgres;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using Daily = Caroline.Commands.Daily;
using LoggerFactory = Caroline.Modules.LoggerFactory;

namespace Caroline
{
    public class Program
    {
        public static IServiceProvider di;

        static readonly ManualResetEvent ResetEvent = new ManualResetEvent(false);
        static CarolineTelegramBot _bot;

        [STAThread]
        private static void Main()
        {
            var servicesProvider = BuildDi();
            di = servicesProvider;

            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;

            Console.OutputEncoding = System.Text.Encoding.UTF8;

            _bot = servicesProvider.GetRequiredService<CarolineTelegramBot>();

            _bot.Start();

            Console.WriteLine("Bot started");
            ResetEvent.WaitOne();
        }

        public static IServiceProvider BuildDi()
        {
            var services = new ServiceCollection();

            //Runner is the custom class
            services.AddTransient<CarolineTelegramBot>();

            services.AddSingleton<Daily>();
            services.AddSingleton<DailyTelegram>();
            services.AddSingleton<Hello>();
            services.AddSingleton<Ping>();
            services.AddSingleton<RandomNumber>();
            services.AddSingleton<Shop>();
            services.AddSingleton<Shrug>();
            services.AddSingleton<Weather>();
            services.AddSingleton<SystemHelp>();
            services.AddSingleton<NewsCommand>();
            services.AddSingleton<Punycode>();

            services.AddSingleton<Interfaces.Modules.ILoggerFactory, LoggerFactory>();
            services.AddSingleton(typeof(Interfaces.Modules.ILogger<>), typeof(Modules.Logger<>));
            services.AddSingleton(typeof(Interfaces.IDataRepository<>), typeof(PostgresDataRepository<>));

            services.AddSingleton<ConfigurationHelper, ConfigurationHelper>();
            services.AddLogging(builder => builder.SetMinimumLevel(LogLevel.Trace));

            var serviceProvider = services.BuildServiceProvider();

            var loggerFactory = serviceProvider.GetRequiredService<ILoggerFactory>();

            //configure NLog
            loggerFactory.AddNLog(new NLogProviderOptions { CaptureMessageTemplates = true, CaptureMessageProperties = true });
            NLog.LogManager.LoadConfiguration("nlog.config");

            return serviceProvider;
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            _bot.Stop();
            Thread.Sleep(2000);
            ResetEvent.Close();
        }
    }
}
